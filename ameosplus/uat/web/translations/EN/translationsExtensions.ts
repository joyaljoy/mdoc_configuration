export const EN_TRANSLATIONS_EXTENSION = {
     AMEOS: {

             AUTH : {
                       PATIENT_WELCOME_TITLE: 'Welcome',
                       PATIENT_WELCOME_DESCRIPTION: 'We wish you a pleasant stay.',
                     },
             DASHBOARD: {
                     WELCOME_MSG: "Welcome.<br><br>We wish you a pleasant stay.",
                      }
        }
}